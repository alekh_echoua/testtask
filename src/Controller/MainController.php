<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class MainController
 */
class MainController extends AbstractController
{
    /**
     * Hello
     *
     * @return Response
     */
    public function helloAction(): Response
    {
        return new Response('Hello!!!');
    }

    /**
     * Hello slug
     *
     * @param string $slug
     *
     * @return Response
     */
    public function helloSlugAction(string $slug): Response
    {
        $output = \sprintf('Hello %s!!!', $slug);

        return new Response($output);
    }

    /**
     * Post action
     *
     * @param Request $request
     *
     * @return Response
     */
    public function postAction(Request $request): Response
    {
        return new Response($request->getContent());
    }
}
