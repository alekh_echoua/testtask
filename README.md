# EchoUA Test task #

## How to install ##

* Clone this repository.
* Execute `composer install` command.
* Create `.env` file based on `.env.dist` file.
* Add `127.0.0.1 echo-test.local` line to `/etc/hosts` file on your local computer.
* Double check that `APP_BASE_PATH` parameter in `.env` file is correct, and contain `/` on the end of line.
* Execute `docker-compose up --build` command.

Now You are able to go to `echo-test.local` domain to check result.

## Available endpoints ##

* GET `http://echo-test.local/` Should output `Hello!!!`.
* GET `http://echo-test.local/hello/{slug}` Should output `Hello {slug}!!!`.
* POST `http://echo-test.local/post` Should output request RAW body content.